﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace TestApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static MainWindow main;
        private string connectionString;
        int[] posId = new int[0], depId = new int[0];

        public MainWindow()
        {
            InitializeComponent();
            connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        /// <summary>
        /// Заполняет DataGrid
        /// </summary>
        private void fillGrid()
        {
            //очищение таблицы
            mainGrid.ItemsSource = null;
            mainGrid.Items.Refresh();

            //объявление таблицы согласно формату возвращаемой таблицы из SQL Server
            DataTable dt = new DataTable();
            dt.Columns.Add("department", typeof(string));
            dt.Columns.Add("position", typeof(string));
            dt.Columns.Add("begin_date", typeof(DateTime));
            dt.Columns.Add("end_date", typeof(DateTime));
            dt.Columns.Add("fod", typeof(int));
            
            //получение id должностей и отделов
            getIds(ref posId, "positions");
            getIds(ref depId, "departments");

            //параметры для хранимой процедуры GetFOD
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@d_id", SqlDbType.Int, 0, "d_id");
            param[1] = new SqlParameter("@p_id", SqlDbType.Int, 0, "p_id");
            param[2] = new SqlParameter("@b_date", SqlDbType.Date, 0, "begin_date");
            param[3] = new SqlParameter("@e_date", SqlDbType.Date, 0, "end_date");

            //для работы с БД
            SqlConnection sqlConnection1 = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader = null;            

            cmd.Parameters.AddRange(param);
            cmd.CommandText = "sp_GetFDO";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = sqlConnection1;
            sqlConnection1.Open();

            try
            {
                //установление границ выборки по датам
                param[2].Value = dpBegin.Text;
                param[3].Value = dpEnd.Text;
                foreach (int p in posId)
                    foreach (int d in depId)
                    {
                        //перебор по всем должностям и отделам
                        param[0].Value = d;
                        param[1].Value = p;

                        //добавление результата работы процедуры в DataGrid
                        reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                DataRow row = dt.NewRow();
                                row["department"] = reader.GetString(0);
                                row["position"] = reader.GetString(1);
                                row["begin_date"] = reader.GetDateTime(2);
                                row["end_date"] = reader.GetDateTime(3);
                                row["fod"] = reader.GetInt32(4);
                                dt.Rows.Add(row);
                            }
                        }
                        reader.Close();
                   }
            } catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            } finally
            {
                if(reader!=null)
                    reader.Close();
                if(sqlConnection1!=null)
                    sqlConnection1.Close();
            }
            mainGrid.ItemsSource = dt.DefaultView;
        }

        /// <summary>
        /// получает Id записи
        /// </summary>
        /// <param name="arr">массив с id</param>
        /// <param name="from">имя таблицы</param>
        public void getIds(ref int[] arr, string from)
        {
            string sql = "SELECT * FROM " + from;
            SqlDataAdapter adapter;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    DataSet dsPositions = new DataSet();
                    adapter.Fill(dsPositions);

                    List<int> idList = new List<int>();

                    foreach (DataRow row in dsPositions.Tables[0].Rows)
                    {
                        idList.Add(Convert.ToInt32(row["id"].ToString()));
                    }

                    arr = idList.ToArray();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void rateButton_Click(object sender, RoutedEventArgs e)
        {
            Rate rate = new Rate();
            rate.Show();
        }

        private void buldButton_Click(object sender, RoutedEventArgs e)
        {
            fillGrid();
        }

        private void departmentButton_Click(object sender, RoutedEventArgs e)
        {
            Department dep = new Department();
            dep.Show();
        }
    }
}
