﻿using System;
using System.Windows;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace TestApp
{
    /// <summary
    /// Логика взаимодействия для Rate.xaml
    /// </summary>
    public partial class Rate : Window
    {
        string connectionString;
        DataSet dsRates;
        SqlConnection connection;
        SqlDataAdapter adapter;

        public Rate()
        {
            InitializeComponent();
            connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        /// <summary>
        /// Заполнение ComboBox данными из таблицы Должность
        /// </summary>
        private void fillCheckBox()
        {
            string sql = "SELECT * FROM positions";
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    DataSet dsPositions = new DataSet();
                    adapter.Fill(dsPositions);

                    cbPosition.DataContext = dsPositions.Tables[0].DefaultView;
                    cbPosition.SelectedValuePath = "id";
                    cbPosition.DisplayMemberPath = "position";
                    cbPosition.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Close();
            }
        }

        /// <summary>
        /// Заполнение DataGrid данными из таблицы Ставки
        /// </summary>
        private void fillGrid()
        {
            string sql = @"
                SELECT positions.position, init_date, rate 
                FROM rates
                JOIN positions ON rates.position_id = positions.id";
            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();
                adapter = new SqlDataAdapter(sql, connection);
                adapter.InsertCommand = new SqlCommand("sp_InsertRate", connection);
                adapter.InsertCommand.CommandType = CommandType.StoredProcedure;
                adapter.InsertCommand.Parameters.Add(new SqlParameter("@position", SqlDbType.NChar, 30, "position"));
                adapter.InsertCommand.Parameters.Add(new SqlParameter("@init_date", SqlDbType.Date, 0, "init_date"));
                adapter.InsertCommand.Parameters.Add(new SqlParameter("@rate", SqlDbType.Int, 0, "rate"));

                SqlParameter parameter = adapter.InsertCommand.Parameters.Add("@id", SqlDbType.Int, 0, "id");
                parameter.Direction = ParameterDirection.Output;

                dsRates = new DataSet();
                adapter.Fill(dsRates);
                rateGrid.ItemsSource = dsRates.Tables[0].DefaultView; 
                adapter.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Close();
            }
        }

        /// <summary>
        /// Добавление данных в таблицу Ставок
        /// </summary>
        private void insertRate()
        {
            DataTable dt = dsRates.Tables[0];

            DataRow newRow = dt.NewRow();
            try
            {
                newRow["position"] = cbPosition.Text;
                newRow["init_date"] = datePicker.Text;
                newRow["rate"] = tbRate.Text;
                dt.Rows.Add(newRow);

                adapter.Update(dsRates);
                dsRates.AcceptChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            insertRate();
        }        

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            fillCheckBox();
            fillGrid();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (connection != null)
                connection.Close();
        }
    }
}
