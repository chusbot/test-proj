﻿using System;
using System.Windows;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Windows.Controls;

namespace TestApp
{
    /// <summary>
    /// Логика взаимодействия для Department.xaml
    /// </summary>
    public partial class Department : Window
    {
        SqlConnection connection;
        SqlDataAdapter adapter;
        string connectionString;
        DataSet dsShedule;

        public Department()
        {
            InitializeComponent();
            connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        /// <summary>
        /// Заполнение ComboBox
        /// </summary>
        /// <param name="cb">ComboBox</param>
        /// <param name="tableName">Имя таблицы из которой происходит выборка данных</param>
        /// <param name="columnName">Имя столбца в DataGrid</param>
        private void fillCheckBox(ComboBox cb, string tableName, string columnName)
        {
            string sql = "SELECT * FROM " + tableName;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    adapter = new SqlDataAdapter(sql, connection);

                    DataSet ds = new DataSet();
                    adapter.Fill(ds);

                    cb.DataContext = ds.Tables[0].DefaultView;
                    cb.SelectedValuePath = "id";
                    cb.DisplayMemberPath = columnName;
                    cb.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                adapter.Dispose();
            }
        }

        /// <summary>
        /// Заполняет DataGrid данными из таблицы штатного расписания
        /// </summary>
        private void fillGrid()
        {
            string sql = @"
                SELECT departments.department, positions.position, init_date, employee_count 
                FROM shedule
                JOIN departments ON shedule.department_id = departments.id
                JOIN positions ON shedule.position_id = positions.id";
            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();
                adapter = new SqlDataAdapter(sql, connection);
                adapter.InsertCommand = new SqlCommand("sp_InsertShedule", connection);
                adapter.InsertCommand.CommandType = CommandType.StoredProcedure;
                adapter.InsertCommand.Parameters.Add(new SqlParameter("@department", SqlDbType.NChar, 30, "department"));
                adapter.InsertCommand.Parameters.Add(new SqlParameter("@position", SqlDbType.NChar, 30, "position"));
                adapter.InsertCommand.Parameters.Add(new SqlParameter("@init_date", SqlDbType.Date, 0, "init_date"));
                adapter.InsertCommand.Parameters.Add(new SqlParameter("@employee_count", SqlDbType.Int, 0, "employee_count"));

                SqlParameter parameter = adapter.InsertCommand.Parameters.Add("@id", SqlDbType.Int, 0, "id");
                parameter.Direction = ParameterDirection.Output;

                dsShedule = new DataSet();
                adapter.Fill(dsShedule);
                rateGrid.ItemsSource = dsShedule.Tables[0].DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Добавление данных в таблицу Штатное расписание
        /// </summary>
        private void insertShedule()
        {
            DataTable dt = dsShedule.Tables[0];

            DataRow newRow = dt.NewRow();
            try
            {
                newRow["department"] = cbDepartmen.Text;
                newRow["position"] = cbPosition.Text;
                newRow["init_date"] = datePicker.Text;
                newRow["employee_count"] = tbEmployeeCount.Text;
                dt.Rows.Add(newRow);

                adapter.Update(dsShedule);
                dsShedule.AcceptChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            insertShedule();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            fillCheckBox(cbDepartmen, "departments", "department");
            fillCheckBox(cbPosition, "positions", "position");
            fillGrid();
        }
    }
}
